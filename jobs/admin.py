from django.contrib import admin

# Register your models here.
from jobs.models import Publication, Profile

admin.site.register(Publication)
admin.site.register(Profile)