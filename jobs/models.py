from django.db import models
from rest_framework_jwt.serializers import User

TYPES = (
    ('Full-time', 'FullTime'),
    ('Part-time', 'PartTime')
)


class Publication(models.Model):
    title = models.CharField(max_length=100)
    place = models.CharField(max_length=100)
    date = models.DateField()
    salary = models.DecimalField(decimal_places=2, max_digits=6, null=True, blank=True)
    type = models.CharField(max_length=20, choices=TYPES)
    area = models.CharField(max_length=100)
    description = models.TextField()
    requirements = models.TextField()
    company = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    lastname = models.CharField(max_length=100)
    avatar = models.CharField(max_length=100, null=True, blank=True)
    age = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return '%s %s' %(self.name, self.lastname)


class Application(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='application_user')
    publication = models.ForeignKey(Publication, on_delete=models.CASCADE, related_name='application_publication')
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return '%s %s' %(self.user, self.publication)





