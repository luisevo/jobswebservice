
# Create your views here.
from rest_framework import serializers, viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from .models import Publication, Profile


class PublicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publication
        fields = ('__all__')


# ViewSets define the view behavior.
class PublicationViewSet(viewsets.ModelViewSet):
    queryset = Publication.objects.all()
    serializer_class = PublicationSerializer


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('id', 'name', 'lastname', 'avatar', 'age')


class ProfileViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    http_method_names = ['get', 'put']

    def list(self, request):
        queryset = Profile.objects.get(user=self.request.user)
        serializer = ProfileSerializer(queryset)
        return Response(serializer.data)

