from rest_framework import routers
from django.urls import path, include

from .views import PublicationViewSet, ProfileViewSet

router = routers.DefaultRouter()
router.register('jobs', PublicationViewSet)
router.register('profile', ProfileViewSet, base_name="api_profile")

urlpatterns = [
    path('', include(router.urls)),
]